package Scoring::Engine::SMTP;

use strict;
use warnings;
use 5.010;

use Scoring::Engine::Backend;
use Scoring::Engine::Teams;

use POE qw/Component::Client::SMTP/;
use YAML::Any qw/Load/;

use constant USER_CONFIG => '/users.yml';
use constant SMTP_CONFIG => '/smtp.yml';

# Read config on module load
my ($users, $teams);
{
    local $/;

    my $fs = fs();
    my $fh = $fs->lookup(SMTP_CONFIG)->open("r");
    $teams = Load(<$fh>);
    close($fh);

    $fh = $fs->lookup(USER_CONFIG)->open("r");
    $users = Load(<$fh>);
    close($fh);
}

sub service {
    my ($index, $body, $user);
    foreach my $team (keys %$teams) {
        $index = int(rand(@{$users->{$team}}));
        $body = "hot";
        $user = $users->{$team}->[$index];
        POE::Component::Client::SMTP->send(
            From => 'crts-whiteteam@csh.rit.edu',
            To => "$user\@csh.rit.edu",
            Body => \$body,
            Server => $teams->{$team},
            Timeout => 1000,
            Auth => { 
                user => $user,
                pass => $users->{$team}->[$index]->{pass},
            },
            Context => $team,
            SMTP_Success => 'smtp_response',
        );
    }
}

sub test {
    my $team = $_[ARG0];
    teams()->{$team}->give_points();
    say "Giving $team 1 point for SMTP";
}

1;
