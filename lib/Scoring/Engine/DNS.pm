package Scoring::Engine::DNS;

use strict;
use warnings;
use 5.010;

use Scoring::Engine::Backend;
use Scoring::Engine::Teams;

use POE qw/Component::Client::DNS/;
use YAML::Any qw/Load/;

use constant DNS_CONFIG => '/dns.yml';

# Read config on module load
my $fs = fs();
my $fh = $fs->lookup(DNS_CONFIG)->open("r");
local $/;
my $teams = Load(<$fh>);
close($fh);

my $resolver = POE::Component::Client::DNS->spawn();

# Test DNS queries listed in YAML
sub service {
    my $response;

    foreach my $team (keys %$teams) {
        foreach my $query (@{$teams->{$team}->{domains}}) {
            $response = $resolver->resolve(
                event   => 'dns_response',
                host    => $query,
                context => { team => $team },
            );

            if ($response) {
                $_[KERNEL]->yield(dns_response => $response);
            }
        }

# Reverse queries too
        foreach my $rev (@{$teams->{$team}->{reverses}}) {
            $response = $resolver->resolve(
                event   => 'dns_response',
                host    => $rev,
                context => { team => $team },
                type    => 'PTR',
            );

            if ($response) {
                $_[KERNEL]->yield(dns_response => $response);
            }
        }
    }
}

sub test {
    my $team = $_[ARG0]->{context}->{team};
    foreach my $answer ($_[ARG0]->{response}->answer()) {
        if ($answer and $answer->rdatastr()) {
            teams()->{$team}->give_points();
            say "Giving $team 1 point for DNS";
            last;
        }
    }
}

1;
