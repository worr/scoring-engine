package Scoring::Engine::Backend;

use strict;
use warnings;
use 5.010;

require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT = qw/fs/;

use Ouch;

sub fs {
    state $fs;

    if ($Test::Scoring::Engine::test) {
        require Scoring::Engine::Backend::Test;
        $fs = &Scoring::Engine::Backend::Test::fs;
        return $fs;
    }

    given ($^O) {
        when ("MSWin32") {
            require Scoring::Engine::Backend::Windows;
            $fs = &Scoring::Engine::Backend::Windows::fs;
        }

        when ("linux") {
            require Scoring::Engine::Backend::Linux;
            $fs = &Scoring::Engine::Backend::Linux::fs;
        }

        when ("darwin") {
            require Scoring::Engine::Backend::Mac;
            $fs = &Scoring::Engine::Backend::Mac::fs;
        }
    }


    return $fs;
}

1;
