package Scoring::Engine::Backend::Mac;

use strict;
use warnings;
use 5.010;

use File::System;
use Ouch;

use constant CONF => "$ENV{'HOME'}/.scoringengine/";

sub fs {
    my $fs;
    eval {
        $fs = File::System->new('Real', 
            root => CONF);
    } or say bleep and exit(404);
    return $fs;
}

1;
