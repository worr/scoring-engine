package Scoring::Engine::Backend::Test;

use strict;
use warnings;
use 5.010;

use File::System;
use Cwd;

use constant CONF => cwd()."/t/configs/";

sub fs {
    my $fs;
    eval {
        $fs = File::System->new('Real',
            root => CONF);
    } or say bleep and exit(404);
    return $fs;
}

1;
