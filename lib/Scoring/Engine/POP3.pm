package Scoring::Engine::POP3;

use strict;
use warnings;
use 5.010;

use POE qw/Component::Client::POP3/;
use YAML::Any qw/Load/;

use Scoring::Engine::Backend;
use Scoring::Engine::Teams;

use constant USER_CONFIG => '/users.yml';
use constant POP3_CONFIG => '/pop3.yml';

# Read config on module load
my ($users, $teams);
{
    local $/;

    my $fs = fs();
    my $fh = $fs->lookup(POP3_CONFIG)->open("r");
    $teams = Load(<$fh>);
    close($fh);

    $fh = $fs->lookup(USER_CONFIG)->open("r");
    $users = Load(<$fh>);
    close($fh);
}

sub service {
    my $index;
    foreach my $team (keys %$teams) {
        $index = int(rand(@{$users->{$team}}));
        POE::Component::Client::POP3->spawn(
            Alias => "pop_$team",
            Username => $users->{$team}->[$index]->{user},
            Password => $users->{$team}->[$index]->{pass},
            AuthMethod => 'PASS',
            RemoteAddr => $teams->{$team},
            Events => [ qw/connected authenticated/ ],
        );
    }
}

sub authenticated {
    my ($team) = $_[SENDER]->[0]->{alias} =~ /(?:pop|ftp)_(\w+)/;
    say "Giving $team 1 point for POP3 / FTP";
    teams()->{$team}->give_points();
}
