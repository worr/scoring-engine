package Scoring::Engine::PointListener;

use strict;
use warnings;
use 5.010;

use Scoring::Engine::Teams; 

require Exporter;
use IO::Socket;
use POE qw/Wheel::SocketFactory Wheel::ReadWrite/;

our @ISA = qw/Exporter/;
our @EXPORT = qw/
    start_plistener 
    stop_plistener 
    on_client_accept 
    on_server_error
    on_client_input
    on_client_error
/;

sub start_plistener {
    unlink '/tmp/plistener' if (-e '/tmp/plistener');
    $_[HEAP]{server} = POE::Wheel::SocketFactory->new(
        SocketDomain => AF_UNIX,
        BindAddress  => '/tmp/plistener',
        SuccessEvent => 'on_client_accept',
        FailureEvent => 'on_server_error',
    );
}

sub stop_plistener {
    say STDERR "Point server going down...";
    delete $_[HEAP]{server};
    unlink('/tmp/plistener');
}

sub on_client_input {
    my ($input, $wheel_id) = @_[ARG0, ARG1];
    if ($input =~ /^dc /) { 
        delete $_[HEAP]{client}{$wheel_id};
        return;
    }

    my ($team, $points) = $input =~ /(\w+)\s+(\-?\d+)/;
    teams()->{$team}->give_points($points);
}

sub on_client_accept {
    say "Accepted connection from client...";
    my $client_sock = $_[ARG0];
    my $io_wheel = POE::Wheel::ReadWrite->new(
        Handle     => $client_sock,
        InputEvent => 'on_client_input',
        ErrorEvent => 'on_client_error',
    );
    $_[HEAP]{client}{$io_wheel->ID()} = $io_wheel;
}

sub on_server_error {
     my ($operation, $errnum, $errstr) = @_[ARG0, ARG1, ARG2];
     say STDERR "Server $operation error $errnum: $errstr\n";
     delete $_[HEAP]{server};

}

sub on_client_error {
    delete $_[HEAP]{client}{$_[ARG3]};
}

1;
