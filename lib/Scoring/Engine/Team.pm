package Scoring::Engine::Team;


use strict;
use warnings;
use 5.012;

# Moose is so overkill for this
# I have no need for inheritance or other bullshit
sub new {
    my $self = { POINTS => 0 };
    return bless($self);
}

sub give_points {
    my $self = shift;
    my $points = shift if (@_);
    unless ($points) {
        $self->{POINTS} += 1;
        return;
    };

    $self->{POINTS} += $points;
}

1;
