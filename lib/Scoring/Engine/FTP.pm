package Scoring::Engine::FTP;

use strict;
use warnings;
use 5.010;

use POE qw/Component::Client::FTP/;
use YAML::Any qw/Load/;

use Scoring::Engine::Backend;
use Scoring::Engine::Teams;

use constant USER_CONFIG => '/users.yml';
use constant FTP_CONFIG => '/ftp.yml';

# Read config on module load
my ($users, $teams);
{
    local $/;

    my $fs = fs();
    my $fh = $fs->lookup(FTP_CONFIG)->open("r");
    $teams = Load(<$fh>);
    close($fh);

    $fh = $fs->lookup(USER_CONFIG)->open("r");
    $users = Load(<$fh>);
    close($fh);
}

sub service {
    my $index;
    foreach my $team (keys %$teams) {
        $index = int(rand(@{$users->{$team}}));
        POE::Component::Client::FTP->spawn(
            Alias => "ftp_$team",
            Username => $users->{$team}->[$index]->{user},
            Password => $users->{$team}->[$index]->{pass},
            RemoteAddr => $teams->{$team},
            Events => [qw/
                authenticated
            /]
        );
    }
}
