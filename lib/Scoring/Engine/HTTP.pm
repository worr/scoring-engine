package Scoring::Engine::HTTP;

use strict;
use warnings;
use 5.010;

use Scoring::Engine::Backend;
use Scoring::Engine::Teams;

use HTTP::Request;
use POE qw/Component::Client::HTTP/;
use YAML::Any qw/Load/;

use constant HTTP_CONFIG => '/http.yml';

# Read config on module load
my $fs = fs();
my $fh = $fs->lookup(HTTP_CONFIG)->open("r");
local $/;
my $teams = Load(<$fh>);
close($fh);

my $http = POE::Component::Client::HTTP->spawn(Alias => 'ua');

sub service {
    foreach my $team (keys %$teams) {
        foreach my $site (@{$teams->{$team}->{sites}}) {
            my $req = HTTP::Request->new(GET => $site);
            $req->header(team => $team);

            $_[KERNEL]->post(
                'ua', 'request',
                'http_response',
                $req
            );
        }
    }
}

sub test {
    my ($req, $res) = @_[ARG0, ARG1];
    my $team = $req->[0]->header('team');
    
    if ($res->[0]->is_success()) {
        teams()->{$team}->give_points();
        say "Giving $team 1 point for HTTP";
    }
}

1;
