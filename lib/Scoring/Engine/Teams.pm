package Scoring::Engine::Teams;

use strict;
use warnings;
use 5.010;

use Scoring::Engine::Backend;
use Scoring::Engine::Team;

require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT = qw/teams/;

use YAML::Any qw/Load/;

use constant TEAM_CONFIG => '/teams.yml';

state $team_refs;

sub init {
    my $fs = Scoring::Engine::Backend::fs;
    my $fh = $fs->lookup(TEAM_CONFIG)->open('r');
    local $/;
    my $teams = Load(<$fh>);
    close($fh);

    foreach my $team (@$teams) {
        $team_refs->{$team} = Scoring::Engine::Team->new;
    }

    return $team_refs;
}

sub teams {
    $team_refs //= init;
    return $team_refs;
}

1;
