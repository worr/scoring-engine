#!/usr/bin/perl

# ABSTRACT: Scores defense-based hacking competitions

use strict;
use warnings;
use 5.010;

use Scoring::Engine::PointListener;
use Scoring::Engine::Team;
use Scoring::Engine::Teams;

use DBI;
use POE;
use YAML::Any qw/LoadFile DumpFile/;

require Exporter;

our @ISA = qw/Exporter/;
our @EXPORT = qw/start_engine/;

my @service_names = (
    'dns',
    'ftp',
    'http',
    'pop3',
    'smtp',
);

sub start {
    $_[KERNEL]->sig(INT => 'sigint_handler');
    $_[KERNEL]->yield('start_plistener');

    if (-f 'scores') {
        my $teams = LoadFile('scores');
        foreach my $team (keys %$teams) {
            teams()->{$team}->give_points($teams->{$team});
        }
    }

    $_[KERNEL]->yield('tick');
}

sub stop {
    say "\n\nResults:\n--------";
    foreach my $team (keys %{teams()}) {
        say "$team: ".teams()->{$team}->{POINTS};
    }

    $_[KERNEL]->yield('stop_plistener');
}

sub tick {
    say "Running tests...";
    $_[KERNEL]->yield('file_dump');
    #$_[KERNEL]->yield('db_dump');
    $_[KERNEL]->yield($_) foreach (@service_names);
    $_[KERNEL]->delay(tick => 60) if not ($Test::Scoring::Engine::test);
}

sub db_dump {
    my $dbh = DBI->connect("dbi:SQLite:dbname=dump.sql","","");
    my $sth;
    foreach my $team (keys %{teams()}) {
        $sth = $dbh->prepare("INSERT INTO crts VALUES (?, ?, ?, ?);");
        $sth->bind_param(1, undef);
        $sth->bind_param(2, $team);
        $sth->bind_param(3, time);
        $sth->bind_param(4, teams()->{$team}->{POINTS});
        $sth->execute;
    }
    $dbh->commit();
    $dbh->disconnect();
}

sub file_dump {
    my %teams;
    foreach my $team (keys %{teams()}) {
        $teams{$team} = teams()->{$team}->{POINTS};
    }

    DumpFile('scores', \%teams);
}

sub start_engine {
    my %poe_states = (
        start_plistener => \&start_plistener,
        stop_plistener => \&stop_plistener,
        on_client_accept => \&on_client_accept,
        on_client_error => \&on_client_error,
        on_client_input => \&on_client_input,
        on_server_error => \&on_server_error,
        file_dump => \&file_dump,
        #db_dump => \&db_dump,
        _start => \&start,
        tick => \&tick,
        _stop => \&stop,
        @_
    );

    POE::Session->create( inline_states => \%poe_states );
    POE::Kernel->run();
}

sub sigint_handler {
    $_[KERNEL]->alarm_remove_all();
    $_[KERNEL]->signal_handled();
}

1;
