#!/usr/bin/env perl

use strict;
use warnings;
use 5.010;

use Scoring::Engine;
use Scoring::Engine::DNS;
use Scoring::Engine::FTP;
use Scoring::Engine::HTTP;
use Scoring::Engine::POP3;
use Scoring::Engine::SMTP;

say "CRTs Scoring Engine\n";

my %services = (
    dns =>              \&Scoring::Engine::DNS::service,
    dns_response =>     \&Scoring::Engine::DNS::test,
    http =>             \&Scoring::Engine::HTTP::service,
    http_response =>    \&Scoring::Engine::HTTP::test,
    smtp =>             \&Scoring::Engine::SMTP::service,
    smtp_response =>    \&Scoring::Engine::SMTP::test,
    pop3 =>             \&Scoring::Engine::POP3::service,
    authenticated =>    \&Scoring::Engine::POP3::authenticated,
    ftp  =>             \&Scoring::Engine::FTP::service,
);

foreach my $service (keys %services) {
    say "Checking service $service..." if ($service !~ /_response$/);
}

start_engine(%services);
