#!/usr/bin/env perl

use strict;
use warnings;
use 5.010;

use IO::Socket;
use IO::Socket::UNIX;
use YAML::Any qw/Load/;

use Scoring::Engine::Backend qw/fs/;

my $sock = IO::Socket::UNIX->new('/tmp/plistener') or die "Cannot open socket";
my ($team, @teams, $err, $points);

{
    my $fs = fs;
    my $fh = $fs->lookup('/teams.yml')->open('r') or die "Could not open team config file";
    local $/;
    @teams = @{Load(<$fh>)};
    close($fh);
}

do {
    print "What team should I give points to? ";
    $team = <STDIN>;
    chomp $team;

    unless ($team ~~ @teams) {
        say STDERR "Could not find team in config";
        $err = 1;
    }

    print "How many points? ";
    $points = <STDIN>;
    chomp $points;

    if ($points !~ /\-?\d+/ and $team ne "dc") {
        say STDERR "Invalid number of points";
        $err = 1;
    }

    if ($sock->connected() and not $err) {
        $sock->send("$team $points\n", 0);
    } else {
        say "Could not send data";
    }
} while ($team ne "dc"); 

$sock->close();
