#!/usr/bin/perl

use strict;
use warnings;
use 5.012;

use Test::More tests => 2;
use Test::Scoring::Engine;

use Scoring::Engine::Team;
use Scoring::Engine::Teams qw/teams/;

my $team_refs = teams();
my $correct_refs = {
    team1 => Scoring::Engine::Team->new,
    team2 => Scoring::Engine::Team->new,
    team3 => Scoring::Engine::Team->new,
};

is_deeply($team_refs, $correct_refs, 'testing teams stuff');
$team_refs->{team1}->give_points;
$correct_refs->{team1}->give_points;

$team_refs = teams();
is_deeply($team_refs, $correct_refs, 'testing teams reinvocation');
