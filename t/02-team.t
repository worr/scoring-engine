#!/usr/bin/perl

use strict;
use warnings;
use 5.012;

use Test::More tests => 4;

use Scoring::Engine::Team;

my $team = Scoring::Engine::Team->new;

isa_ok($team, 'Scoring::Engine::Team');
is($team->{POINTS}, 0, 'testing initial points');
$team->give_points;
is($team->{POINTS}, 1, 'testing added points');
$team->give_points(80);
is($team->{POINTS}, 81, 'testing multiple points');
