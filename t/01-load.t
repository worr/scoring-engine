#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 1;
use Test::Scoring::Engine;

use Scoring::Engine;

BEGIN { use_ok('Scoring::Engine'); }
