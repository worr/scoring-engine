=pod

=head1 Scoring::Engine

Automated service checker for defense-based hacking competitions

=head2 Wait what?

Scoring::Engine is designed to be an automated service checking daemon
written in Perl to check for service uptime. It's fully configurable, and
grants points every minute a service is up. It makes heavy use of POE.

=head2 Neat, but where do we see the score?

There should be a coming Dancer-based webapp that displays some graphs that this
program eventually will poop out. That part isn't written yet. :)

=cut
